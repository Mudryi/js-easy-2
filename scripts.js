var Loader = (function() {
      'use strict';
      return {
         init: function() {
            /* get elements*/
            this.loader = document.getElementById("loader");
            this.progressBar = document.getElementById("loaderProgressBar");
            this.loaderPercent = document.getElementById("loaderPercent");
            this.images = document.getElementsByTagName("img");
            
            /* add additional properties below if needed */
            this.allImagesCount = this.images.length;
            this.imageCounter = 0;
           
            this.addEvents();
         },

          addEvents: function() {
        	 var self = this;
        	 
        	 for (var i = 0; i < self.allImagesCount; i++) {

        	     /* Awful hack for disabling browser images caching */
        	     self.images[i].src = self.images[i].src;

        		 self.images[i].addEventListener("load", self.increaseProgressBar.bind(self));
        		 self.images[i].addEventListener("error", self.increaseProgressBar.bind(self));
			}
         },

         increaseProgressBar: function() {
        	 this.imageCounter++;
        	 
        	 var percentValue = (this.imageCounter / this.allImagesCount).toFixed(2) * 100 + '%';

        	 this.loaderPercent.innerHTML = percentValue;
        	 this.progressBar.style.width = percentValue;
        	 
        	 if(this.imageCounter === this.allImagesCount) {
        		 setTimeout((this.loadedCallback).bind(this), 500);
        	 }
         },
 
         loadedCallback: function() {
        	 this.loader.remove();
         }
      };
   })();

   Loader.init();